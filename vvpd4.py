def lighten(color, percent):
    a = []
    n = 2
    arr = [color[i:i + n] for i in range(0, len(color), n)]
    for x in arr:
        x = int(x, 16)
        new_x = round(x - (x * percent) / 100)
        if new_x == 0:
            a.append('0')
        a.append(hex(int(new_x))[2:].upper())
    print(''.join(a))


def darken(color, percent):
    a = []
    n = 2
    arr = [color[i:i + n] for i in range(0, len(color), n)]
    for x in arr:
        x = int(x, 16)
        if x == 0:
            new_x = round(x + (int('FF', 16) * percent) / 100)
        else:
            new_x = round(x + ((x * percent) / 100))
        if len((hex(new_x)[2:].upper())) > 2:
            new_x = 255
        a.append(hex(new_x)[2:].upper())
    str_a = ''.join(a)
    print(str_a)


def main():
    while True:
        color = input('Введите цвет: ').upper()
        for i in color:
            if i not in '0123456789ABCDEF':
                print('Неверный цвет', end='\n\n')
                exit()
        if len(color) != 6:
            print('Ошибка', end='\n\n')
            continue
        percent = input('Введите процент: ')
        try:
            int(percent)
        except ValueError:
            print('Ошибка, введите число', end='\n\n')
            continue
        if int(percent) not in range(0, 101):
            print('Ошибка', end='\n\n')
            continue
        print('[1] Осветлить')
        print('[2] Утемнить')
        choice = input('Выберите действие: ')
        if choice in '12':
            if choice == '1':
                lighten(color, int(percent))
            elif choice == '2':
                darken(color, int(percent))
        else:
            print('Введено недопустимое действие', end='\n\n')
            continue


if __name__ == '__main__':
    main()
